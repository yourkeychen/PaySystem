package com.so206.po.ext;

import com.so206.po.SysFunction;
import com.so206.po.SysRole;

import java.util.ArrayList;
import java.util.List;

public class SysRoleExt extends SysRole {

    private List<SysFunction> functions = new ArrayList<>();

    public List<SysFunction> getFunctions() {
        return functions;
    }

    public void setFunctions(List<SysFunction> functions) {
        this.functions = functions;
    }

}
