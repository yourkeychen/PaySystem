package com.so206.mapper.ext;

import com.so206.po.ext.SysSettExt;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysSettExtMapper {

    List<SysSettExt> findpage(@Param("sid") String sid,
                              @Param("status") Integer status,
                              @Param("type") Integer type,
                              @Param("uid") Integer uid);

}
