package com.so206.mapper;

import com.so206.po.SystemDomain;
import com.so206.po.SystemDomainExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SystemDomainMapper {
    long countByExample(SystemDomainExample example);

    int deleteByExample(SystemDomainExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SystemDomain record);

    int insertSelective(SystemDomain record);

    List<SystemDomain> selectByExample(SystemDomainExample example);

    SystemDomain selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SystemDomain record, @Param("example") SystemDomainExample example);

    int updateByExample(@Param("record") SystemDomain record, @Param("example") SystemDomainExample example);

    int updateByPrimaryKeySelective(SystemDomain record);

    int updateByPrimaryKey(SystemDomain record);
}