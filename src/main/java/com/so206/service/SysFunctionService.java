package com.so206.service;

import com.so206.po.SysFunction;
import com.so206.po.ext.SysFunctionExt;

import java.util.List;

public interface SysFunctionService {

    SysFunction find_by_id(Integer id);

    SysFunction saveFunction(SysFunction function);

    SysFunction updateFunction(SysFunction function);

    int delete_by_id(Integer id);

    List<SysFunction> findByParentId(Integer id);

    List<SysFunction> findAllFunctions();

//    List<SysFunctionExt> findMenuByRoleId(Integer role_id);

    List<SysFunctionExt> findMenuByRoleId2(Integer role_id,Integer pid);
}
