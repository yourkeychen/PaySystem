package com.so206.crond;

import com.so206.controller.pay.process.UserBalnesChange;
import com.so206.crond.process.SettThread;
import com.so206.po.SystemWebWithBLOBs;
import com.so206.service.SettLementService;
import com.so206.service.UserService;
import com.so206.service.VipService;
import com.so206.service.WebConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 定时任务 - 结算 - 每晚9:00
 */
@Component
public class SettLementTask {

    private static Logger logger = LoggerFactory.getLogger(SettLementTask.class);

    @Autowired
    private SettLementService settLementService;

    @Autowired
    private UserService userService;

    @Autowired
    private VipService vipService;

    @Autowired
    private WebConfigService webConfigService;

    @Autowired
    private UserBalnesChange userBalnesChange;

    public SettLementTask() {
        logger.info("SettLementTask Init...");
    }

    /**
     * 定时任务 - 结算 - 每晚9:00
     */
    @Async
    @Scheduled(cron = "0 00 21 * * ?")
    public void timerToNow() {
        logger.info("当前时间:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        SystemWebWithBLOBs config = webConfigService.find_by_id(1);
        if (config.getAutoSett() == 1) {
            SettThread thread = new SettThread(settLementService, userService,
                    vipService, webConfigService, userBalnesChange);
            Thread thread1 = new Thread(thread);
            thread1.setDaemon(true);
            thread1.start();
        }
    }

}
